generations = cell (num_generations, 1);
ranking = rank_generation (ga, 'fitness');
generations{1} = ga(ranking);

for i = 2:num_generations
  generations{i} = new_generation (generations{i-1});
end

ans = print_generations (generations);
disp (ans);
