global train;
global route;

%% Values to change
num_genomes = 6;
num_variables = 3;

ga = cell (num_genomes, 1);

gen = struct ('control', 1, ...
	      'braking', 0.7, ...
	      'gain', 0.9, ...
	      'genome', '0', ...
	      'scheme', '4d', ...
	      'energy_consumed', 0.0, ...
	      'journey_time', 0.0);

i = 1;

while i <= num_genomes
  ga{i} = gen;
  gen.control = randi(3);
  gen.braking = rand;
  gen.gain = rand * 2;

  [genome_1, scheme_1] = encode_integer (gen.control, 3);
  [genome_2, scheme_2] = encode_number_digit (gen.braking, 1, 4);
  [genome_3, scheme_3] = encode_number_digit (gen.gain, 1, 4);
  genome = strcat (genome_1, genome_2, genome_3);
  scheme = strcat (scheme_1, '|',  scheme_2, '|', scheme_3);

  gen.genome = genome;
  gen.scheme = scheme;

  for j = 1:i - 1
    if (ga{i}.genome == ga{j}.genome)
      continue
    end
  end


  [fitness_x, fitness_y] = single_train_simulator (train, route, ...
						   gen.control, gen.braking, ...
						   gen.gain);


  gen.energy_consumed = fitness_x;
  gen.journey_time = fitness_y;
  gen.fitness = evaluate_genome (gen, @evaluate_genome_balanced);
  ga{i} = gen;
  i = i + 1;
end

disp ("Initialisation completed.");
