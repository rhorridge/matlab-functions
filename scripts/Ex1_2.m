%%%%  Copyright (c) 2018 Richard Horridge and Ashley Turner
%%%%
%%%%  This file is part of `matlab-functions' - Helpful functions for
%%%%  Matlab and GNU Octave.


matM = [3 4 5 2 3 6;
12 95 23 0 29 39;
23 64 72 41 8 91;
47 28 31 62 84 23;
28 40 39 45 54 59];

C = top_left_bottom_right (matM, 23);
disp (C);
disp (C{1});
disp (C{2});
