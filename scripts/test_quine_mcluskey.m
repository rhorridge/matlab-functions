%%%%  Copyright (C) 2019 Richard Horridge <rhorridge@hotmail.co.uk>
%%%%
%%%%  This file is part of `matlab-functions' - Helpful functions for
%%%%  Matlab and GNU Octave.

addpath ("../functions");
pkg load statistics;

dont_care = [];

%%%% A
variable_number = 7;
minterm = create_number_array ( ...
				"0111100", ... # fn (0111, n_d)
				"1111010", ... # fn (1111, n_e)
				"1111000", ... # fn (1111, s_e)
				"1111100", ... # ...
				"1110001", ... # fn (1110, n_f)
				"1110000", ... # fn (1110, s_f)
				"1110010", ... # ...
				"1100000", ... # fn (1100, s_g)
				"1100001"  ... # ...
			      );

output_A_1 = quine_mcluskey (variable_number, minterm, dont_care);
output_A = quine_mcluskey_alg (variable_number, minterm, dont_care);

%%%% B
variable_number = 10;
minterm = create_number_array ( ...
				"0011010000", ... # fn (0011, n_c)
				"0111100000", ... # fn (0111, n_d)
				"0111000000", ... # fn (0111, s_d)
				"0111010000", ... # ...
				"1111000010", ... # fn (1111, n_e)
				"1111000000", ... # fn (1111, s_e)
				"1111100000", ... # ...
				"1110000001", ... # fn (1110, n_f)
				"1110000000", ... # fn (1110, s_f)
				"1110000010", ... # ....
				"1100000000", ... # fn (1100, s_g)
				"1100000001"  ... # ...
				);


output_B_1 = quine_mcluskey (variable_number, minterm, dont_care);
output_B = quine_mcluskey_alg (variable_number, minterm, dont_care);

%%%% C
variable_number = 9;
minterm = create_number_array ( ...
				"000100010", ... # fn (0001, n_b)
				"001101000", ... # fn (0011, n_c)
				"001100000", ... # fn (0011, s_c)
				"001100010", ... # ...
				"011110000", ... # fn (0111, n_d)
				"011100000", ... # fn (0111, s_d)
				"011101000", ... # ...
				"111100001", ... # fn (1111, n_e)
				"111100000", ... # fn (1111, s_e)
				"111110000", ... # ...
				"111000000", ... # fn (1110, s_f)
				"111000001"  ... # ...
			      );

output_C_1 = quine_mcluskey (variable_number, minterm, dont_care);
output_C = quine_mcluskey_alg (variable_number, minterm, dont_care);

%%%% D
variable_number = 8;
minterm = create_number_array ( "00000010", ... # fn (0000, n_a)
				"00010001", ... # fn (0001, n_b)
				"00010000", ... # fn (0001, s_b)
				"00010010", ... # ...
				"00110100", ... # fn (0011, n_c)
				"00110000", ... # fn (0011, s_c)
				"00110001", ... # ...
				"01111000", ... # fn (0111, n_d)
				"01110000", ... # fn (0111, s_d)
				"01110100", ... # ...
				"11110000", ... # fn (1111, s_e)
				"11111000"  ... # ...
			      );

disp (minterm);
output_D_1 = quine_mcluskey (variable_number, minterm, dont_care);
output_D = quine_mcluskey_alg (variable_number, minterm, dont_care);

printf ("\nA:\n");
disp (output_A_1);
disp (output_A);
printf ("\nB:\n");
disp (output_B_1);
disp (output_B);
printf ("\nC:\n");
disp (output_C_1);
disp (output_C);
printf ("\nD:\n");
disp (output_D_1);
disp (output_D);
