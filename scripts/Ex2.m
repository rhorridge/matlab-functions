%%%%  Copyright (c) 2018 Richard Horridge and Ashley Turner
%%%%
%%%%  This file is part of `matlab-functions' - Helpful functions for
%%%%  Matlab and GNU Octave.

%% Filter for wave data

[wave, filter_1, filter_2] = setup ();

[wave.data, wave.sampling_frequency] = audioread (wave.filename);

period = 1000 / wave.sampling_frequency; % ms

%%% Raw sound data
handle = setup_plot ();
plot_raw_sound_data_wrapper (wave, 'White Noise');
axis ([500 1000 -1 1]);
save_plot (handle, 'white_noise');

%%% Filter 1
filter_1_result = difference_equation_wrapper (wave, filter_1, 0);
handle = setup_plot ();
plot_raw_sound_data_wrapper (filter_1_result, 'Result of Filter 1');
axis ([500 1000 -1 1]);
save_plot (handle, 'filter_1_result');

%%% Filter 2
filter_2_result = difference_equation_wrapper (filter_1_result, filter_2, 0);
handle = setup_plot ();
plot_raw_sound_data_wrapper (filter_2_result, 'Result of Filter 2');
axis ([500 1000 -1 1]);
save_plot (handle, 'filter_2_result');

%%% Combination of outputs
handle = setup_plot ();
plot_raw_sound_data_wrapper (wave, ...
			     'White Noise, result of Filter 1 and result of Filter 2');
period = to_period (wave.sampling_frequency);
times = [period:period:period * length(wave.data)];
plot (times, filter_1_result.data);
plot (times, filter_2_result.data, 'r', 'linewidth', 2);
axis ([0 5 -1 1]);
legend ('Raw Data', 'Filter 1', 'Filter 2');
save_plot (handle, 'raw_sound_data_all_outputs');

audiowrite ('filter_1.wav', filter_1_result.data, filter_1_result.sampling_frequency);
audiowrite ('filter_2.wav', filter_2_result.data, filter_2_result.sampling_frequency);

setup_plot ();

filter_1_impulse_response = impulse_response (filter_1.coefficients);
filter_2_impulse_response = impulse_response (filter_2.coefficients);

filter_1_mfc = magnitude_frequency_characteristic (filter_1_impulse_response);
filter_2_mfc = magnitude_frequency_characteristic (filter_2_impulse_response);

subplot (2, 1, 1);
plot_magnitude_frequency_characteristic (filter_1_mfc, ...
					 wave.sampling_frequency, ...
					 wave.frequency, 'Filter 1', ...
					 'b');

subplot (2, 1, 2);
plot_magnitude_frequency_characteristic (filter_2_mfc, ...
					 wave.sampling_frequency, ...
					 wave.frequency, 'Filter 2', ...
					 'b');

save_plot (handle, 'magnitude-frequency-characteristics');
