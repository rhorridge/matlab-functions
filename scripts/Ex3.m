%%%%  Copyright (c) 2018 Richard Horridge and Ashley Turner
%%%%
%%%%  This file is part of `matlab-functions' - Helpful functions for
%%%%  Matlab and GNU Octave.

%%  Filter for wave data

[waveform_bird, waveform_flute, frame] = setup ();

waveform_bird = read_waveform (waveform_bird);
waveform_flute = read_waveform (waveform_flute);

handle = setup_plot ();

%% Plot 1
subplot (2, 2, 1);
plot_raw_sound_data_wrapper (waveform_bird, 'Bird');
bird_axis = [xlim() ylim()];

%% Plot 2
subplot (2, 2, 2);
plot_raw_sound_data_wrapper (waveform_flute, 'Flute');
flute_axis = [xlim() ylim()];

sample = init_samples (frame, waveform_bird.sampling_frequency);
sample_bird = sample_waveform (waveform_bird, frame, sample);
sample_bird.spectrogram = spectrogram (sample_bird.data);

%% Plot 3
subplot (2, 2, 3);
plot_spectrogram_wrapper (sample_bird, 'Bird');
axis (bird_axis);

sample = init_samples (frame, waveform_flute.sampling_frequency);
sample_flute = sample_waveform (waveform_flute, frame, sample);
sample_flute.spectrogram = spectrogram (sample_flute.data);

%% Plot 4
subplot (2, 2, 4);
plot_spectrogram_wrapper (sample_flute, 'Flute');
axis (flute_axis);

save_plot (handle, 'bird-and-flute-10');

handle = setup_plot ();
plot_spectrogram_wrapper (sample_bird, 'Bird');
colorbar ();
caxis ([-90 30]);
save_plot (handle, 'bird-spectrogram-10');

handle = setup_plot ();
plot_spectrogram_wrapper (sample_flute, 'Flute');
colorbar ();
caxis ([-90 50]);
save_plot (handle, 'flute-spectrogram-10');

handle = setup_plot ();

frame.length = 60; % ms

%% Plot 7
subplot (2, 2, 1)
plot_raw_sound_data_wrapper (waveform_bird, 'Bird');
bird_axis = [xlim() ylim()];

%% Plot 8
subplot (2, 2, 2);
plot_raw_sound_data_wrapper (waveform_flute, 'Flute');
flute_axis = [xlim() ylim()];

sample = init_samples (frame, waveform_bird.sampling_frequency);
sample_bird = sample_waveform (waveform_bird, frame, sample);
sample_bird.spectrogram = spectrogram (sample_bird.data);

%% Plot 9
subplot (2, 2, 3);
plot_spectrogram_wrapper (sample_bird, 'Bird');
axis (bird_axis);

sample = init_samples (frame, waveform_flute.sampling_frequency);
sample_flute = sample_waveform (waveform_flute, frame, sample);
sample_flute.spectrogram = spectrogram (sample_flute.data);

%% Plot 10
subplot (2, 2, 4);
plot_spectrogram_wrapper (sample_flute, 'Flute');
axis (flute_axis);

save_plot (handle, 'bird-and-flute-60');

handle = setup_plot ();
plot_spectrogram_wrapper (sample_bird, 'Bird');
colorbar ();
caxis ([-90 30]);
save_plot (handle, 'bird-spectrogram-60');

handle = setup_plot ();
plot_spectrogram_wrapper (sample_flute, 'Flute');
colorbar ();
caxis ([-90 50]);
save_plot (handle, 'flute-spectrogram-60');
