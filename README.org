#+TITLE:  MATLAB Functions
#+AUTHOR: Richard Horridge
#+EMAIL:  rhorridge@hotmail.co.uk
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="~/.local/share/css/rhorridge.css">
#+LATEX_CLASS: org-article

* Introduction

  This repository contains miscellaneous MATLAB / GNU Octave
  functions.

* Building

  Clone or download the repository to your local system, and add the
  directory to your MATLAB / GNU Octave instance.

  #+NAME: addpath
  #+BEGIN_SRC octave
  addpath ('matlab-functions');
  #+END_SRC

* License

  This project is licensed under the GNU General Public License
  version 3.
