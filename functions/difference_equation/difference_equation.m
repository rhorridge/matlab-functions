%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function output = difference_equation (input, coefficients, initial)
  %% Compute the difference equation for INPUT with COEFFICIENTS
  %% for INITIAL value, returning OUTPUT.
  %%
  %% If INITIAL is a scalar, its value is propagated back for the entire
  %% length of total delay in the system.
  %%
  %% If INITIAL is a vector, the offset is applied in reverse i.e. for
  %% INITIAL = [1 2 3] and INPUT = [0 1 2 3] the offset vectors will
  %% be:
  %%
  %% 0 1 2 3  n
  %% 3 0 1 2  n - 1
  %% 2 3 0 1  n - 2
  %% 1 2 3 0  n - 3
  number_of_coefficients = length (coefficients);
  length_input = length (input);
  offset = number_of_coefficients - 1;

  if isscalar (initial)
    initial = initial * ones (1, offset);
  end

  %% Ensure that input is padded with zeros, up to the number of coefficients
  if length_input < offset
    input = ensure_vector_padded_with_zeros (input, offset);
  end

  matrix = offset_vector (input, initial);

  %% need to check lengths of both COEFFICIENTS and MATRIX
  size_matrix = size (matrix);
  rows_matrix = size_matrix(1);
  cols_matrix = size_matrix(2);

  if rows_matrix > number_of_coefficients % too many initial conditions
    limit = number_of_coefficients;
  elseif number_of_coefficients > rows_matrix % too many coefficients
    limit = rows_matrix;
  else
    limit = rows_matrix;
  end

  output = zeros (1, cols_matrix);
  for i = 1:limit % each n - 1 value
    output = coefficients(i) * matrix(i,:) + output;
  end
end
