%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function new_wave = difference_equation_wrapper (wave, filter, initial_conditions)
  %%  Wrapper around `difference_equation' which produces a new struct
  %%  as a copy of WAVE, with data calculated from FILTER with
  %%  INITIAL_CONDITIONS.
  new_wave = wave;
  new_wave.data = difference_equation (wave.data, filter.coefficients, ...
				       initial_conditions);
