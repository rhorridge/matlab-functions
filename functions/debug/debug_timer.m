function [time] = debug_timer (start_time)
  %% DEBUG_TIMER  Start and stop a timer for debugging
  %    TIME = DEBUG_TIMER() called with no arguments
  %    starts a new timer and returns the TIME at
  %    which it was started.
  %
  %    TIME = DEBUG_TIMER(START_TIME) stops the timer
  %    started at START_TIME and returns the elapsed
  %    TIME in seconds.
  %
  %    See also: tic, toc

  [db, ~] = dbstack ();
  str(1:length (db)) = ' ';

  if exist ('start_time', 'var')
    time = toc (start_time);
    msg = sprintf ('[DEBG]%sTimer stopped : %.4f s.', str, time);
  else
    time = tic ();
    msg = sprintf ('[DEBG]%sTimer started.', str);
  end

  disp (msg);
end
