function [message] = debug_print (symbol, stacktrace)
  %% DEBUG_PRINT    Print a debug message.
  %    DEBUG_PRINT (SYMBOL) prints the value of SYMBOL along with
  %    other related information.
  %
  %    See also: inputname, mfilename, sprintf, disp

  symbol_name = inputname(1);
  symbol_value = symbol;

  if (~exist ('file', 'var'))
    pre = "";
    mid = "";
    filename = "";
    linenumber = "";
  else
    pre = " - ";
    mid = " : ";
    filename = stacktrace.name;
    linenumber = stacktrace.line;
  end

  message = sprintf ("%s : %s%s%s%s%s", symbol_name, symbol_value, ...
		     pre, filename, mid, linenumber);

  disp (sprintf ("[DBUG] %s", message));
end
