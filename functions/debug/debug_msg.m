function debug_msg (msg)
  %% DEBUG_MSG    Print a debug message
  %%    DEBUG_MSG(MSG) will print MSG if the global value
  %%    DEBUG is set.
  %%
  %% See also: disp

  if exist ('debug', 'var')
    disp (['[DEBG] ' msg]);
  end
end
