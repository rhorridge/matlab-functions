%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function plot_raw_sound_data_wrapper (waveform, title)
  %%  Wrapper around `plot_raw_sound_data' to plot WAVEFORM with TITLE.
  plot_raw_sound_data (waveform.data, ...
		       to_period (waveform.sampling_frequency), ...
		       title, 'b');
