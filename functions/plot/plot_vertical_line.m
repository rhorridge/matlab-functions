%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function plot_vertical_line (x)
  %%  Plot a vertical line at X.
  ylims = get (gca, 'YLim');
  plot ([x x], ylims);
end
