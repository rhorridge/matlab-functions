%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function save_plot (handle, filename)
  %%  Save plot HANDLE to FILENAME as Portable Network Graphics
  %%  (.png), Scaleable Vector Graphics (.svg), Portable Document
  %%  Format (.pdf) and PostScript (.ps).
  saveas (handle, strcat (filename, '.png'), 'png');
  saveas (handle, strcat (filename, '.svg'), 'svg');
  saveas (handle, strcat (filename, '.pdf'), 'pdf');
  saveas (handle, strcat (filename, '.ps'), 'ps');
  print (handle, strcat (filename, '.eps'), '-deps');
