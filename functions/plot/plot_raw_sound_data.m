%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function handle = plot_raw_sound_data (data, period, title, opts)
  %%  Plot DATA against times constructed with PERIOD with formatted
  %%  TITLE, with plot options OPTS. Return HANDLE to graph plotted.
  hold off;
  times = period:period:period * length(data);
  handle = plot (times, data, opts);
  hold on;
  label_graph (strcat ('Raw sound data of', ' ', title), ...
	       'Time / ms', ...
	       'Amplitude / normalised units');
