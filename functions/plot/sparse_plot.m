function [h] = sparse_plot (x, y, y_condition)
  %% SPARSE_PLOT  Plot discontinuous Y data against X
  %    Plot Y against X, subject to Y_CONDITION, which is a
  %    matrix of the same length as Y with values in the
  %    position where Y_CONDITION is satisfied having the
  %    value 1.
  %
  %    See also: plot

  index = find (y_condition == 0);
  y(index) = NaN;
  h = plot (x, y);
end
