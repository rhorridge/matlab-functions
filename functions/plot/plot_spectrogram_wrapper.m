%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function handle = plot_spectrogram_wrapper (sample, title)
  %%  Plot the spectrogram of SAMPLE, with plot TITLE. Returns
  %%  handle to the plot.
  hold off;
  handle = plot_spectrogram (sample.spectrogram, sample.times, ...
		    sample.frequencies);
  hold on;
  label_graph (strcat ('Spectrogram of ', title), ...
	       'Time / ms', ...
	       'Frequency / Hz');
  colormap (winter);
