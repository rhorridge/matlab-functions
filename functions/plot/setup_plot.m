%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function handle = setup_plot ()
  %%  Set up the plot - clear existing plot and return HANDLE to new figure.
  clf;
  handle = gcf;
  set (handle, 'PaperUnits', 'centimeters');
  set (handle, 'PaperPosition', [0 0 20 20]); % needed to keep figure on page
  set (handle, 'PaperSize', [20, 20]);
