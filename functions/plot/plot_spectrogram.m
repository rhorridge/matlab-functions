%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function handle = plot_spectrogram (spectro, times, frequencies)
  %%  Plot complex spectrogram SPECTRO as an image with TIMES on the x
  %%  axis and FREQUENCIES on the y axis. Returns HANDLE to plot.
  handle = imagesc (times, frequencies, transpose (to_decibels (abs (spectro))));
