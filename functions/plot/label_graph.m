function label_graph (graph_title, graph_xlabel, graph_ylabel)
  %% LABEL_GRAPH    Label current graph
  %%    Label current figure with:
  %%    - Title  GRAPH_TITLE
  %%    - Xlabel GRAPH_XLABEL
  %%    - Ylabel GRAPH_YLABEL
  %%
  %%    See also: title, xlabel, ylabel
  title (graph_title);
  xlabel (graph_xlabel);
  ylabel (graph_ylabel);
end
