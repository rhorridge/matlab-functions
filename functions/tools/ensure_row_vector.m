%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function row_vector = ensure_row_vector (vector)
  %% Ensure that VECTOR is a ROW_VECTOR.
  dims = size (vector);
  if (dims(1) > dims(2))
    row_vector = transpose (vector);
  else
    row_vector = vector;
  end
end
