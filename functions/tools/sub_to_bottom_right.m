%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

%%%% (c) 2018 Richard Horridge and Ashley Turner

function retval = sub_to_bottom_right (matrix, row, col)
% Convert a row and column index ROW and COL into a matrix
% from the bottom-right of MATRIX, including (ROW, COL).
% Checks for out of bounds first, returns the empty matrix if true.
    matrix_rows = size (matrix, 1);
    matrix_cols = size (matrix, 2);
    if row > matrix_rows
        if col > matrix_cols
            retval = [];
            return
        end
    end

    rows = row:matrix_rows;
    cols = col:matrix_cols;
    retval = matrix(rows, cols);
end
