%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function offset = offset_vector (input_vector, initial)
  %% For each element of INITIAL, offset INPUT_VECTOR with the last
  %% element(s).
  input_vector = ensure_row_vector (input_vector);

  length_initial = length (initial);
  length_input_vector = length (input_vector);

  offset = zeros(length_initial + 1, length_input_vector);
  offset(1,:) = input_vector;

  %% This is a mess, needs to be better explained
  for i = length_initial:-1:1
    offset(length_initial-i+2,:) = [initial(i:length_initial) input_vector(1:(length_input_vector - length_initial + i - 1))];
  end
end
