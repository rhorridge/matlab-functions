%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

%%%% (c) 2018 Richard Horridge and Ashley Turner

function retval = top_left_bottom_right (matM, inpV)
  %%  Search for INPV in MATM row-by-row. When found, return the upper-left
  %%  part of MATM including INPV and the bottom-right part of MATM excluding
  %%  INPV as a 1x2 cell array.
  rows_matM = size (matM, 1);
  cols_matM = size (matM, 2);
  matrix = transpose (matM);
  index = find (matrix == inpV);
  [row, col] = ind2sub ([cols_matM, rows_matM], index);
  retval = to_cell_array (sub_to_top_left (matM, col, row), ...
                          sub_to_bottom_right (matM, col + 1, row + 1));
end
