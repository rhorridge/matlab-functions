%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function padded_vector = ensure_vector_padded_with_zeros (input_vector, total_number_of_elements)
  %% Ensures INPUT_VECTOR has exactly TOTAL_NUMBER_OF_ELEMENTS; any elements
  %% after the original vector being zero. Returns PADDED_VECTOR.
  length_input_vector = length (input_vector);
  padded_vector = zeros(1, total_number_of_elements);

  if total_number_of_elements < length_input_vector
    padded_vector = input_vector(1:total_number_of_elements);
  else
    padded_vector(1:length_input_vector) = input_vector(1:end);
  end
end
