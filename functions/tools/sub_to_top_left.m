%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

%%%% (c) 2018 Richard Horridge and Ashley Turner

function retval = sub_to_top_left (matrix, row, col)
% Convert a row and column index ROW and COL into a matrix
% from the top left of MATRIX, including (ROW, COL).
    rows = 1:row;
    cols = 1:col;
    retval = matrix(rows, cols);
end
