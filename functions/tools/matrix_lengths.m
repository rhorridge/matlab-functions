%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function [siz, rows, cols] = matrix_lengths (matrix)
  %%  Compute SIZ, ROWS and COLS for MATRIX.
  siz = size (matrix);
  rows = siz(1);
  cols = siz(2);
