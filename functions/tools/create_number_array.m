function output = create_number_array (varargin)
  %% Put numbers together
  output = [];
  for i = varargin
    dec = bin2dec (i{1});
    output = [output dec];
  end
end
