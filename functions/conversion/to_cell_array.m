%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function [cell_array] = to_cell_array (value_1, value_2)
  %% Put VALUE1 and VALUE2 in CELL_ARRAY.
  cell_array = {value_1, value_2};
end
