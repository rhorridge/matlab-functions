%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function period = to_period (frequency)
  %%  Convert FREQUENCY / Hz into PERIOD / ms.
  period = 1000 / frequency;
