%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function decibels = to_decibels (input)
  %% Convert INPUT into DECIBELS.
  decibels = 20 * log10 (input);
end
