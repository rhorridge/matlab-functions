%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function freq = index_to_frequency (sampling_frequency, number_of_samples)
  %%  Generate a row vector FREQ with NUMBER_OF_SAMPLES elements spaced apart
  %%  according to SAMPLING_FREQUENCY.
  freq = [1:number_of_samples];
  freq = (sampling_frequency / number_of_samples) * freq;
