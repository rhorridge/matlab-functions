%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function response = impulse_response (filter_coefficients)
  %%  Compute the Impulse Response (RESPONSE) of FIR filter with
  %%  FILTER_COEFFICIENTS.
  response = difference_equation ([1 0], filter_coefficients, 0);
