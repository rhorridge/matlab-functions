%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function waveform = read_waveform (waveform)
  %%  Read a WAVEFORM given its (WAVEFORM.)filename.
  [waveform.data, waveform.sampling_frequency] = audioread (waveform.filename);
