%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function spect = spectrogram (sampled_input)
  %%  Compute the `spectrogram' of SAMPLED_INPUT, an m x n matrix for which
  %%  successive samples are stored in rows and samples themselves across
  %%  columns.
  %%
  %%  Produces a matrix of time - frequency (up to the sampling frequency) with
  %%  individual elements (complex).
  [size_input, rows_input, cols_input] = matrix_lengths (sampled_input);
  spect = zeros (size_input);
  for i = 1:rows_input
    spect(i,:) = fft (sampled_input(i,:));
  end
