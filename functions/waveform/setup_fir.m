%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

%%%% Implements Finite Impulse Response (FIR) digital filters
function [wave, filter_1, filter_2] = setup ()
  %% Provide initial values for WAVE, FILTER_1 and FILTER_2.

  wave = struct ('filename', "", ...
		 'frequency', 0, ...
		 'data', [], ...
		 'sampling_frequency', 0);

  filter_1 = struct ('coefficients', []);

  filter_2 = filter_1;

  wave.filename = 'sineWave800Hz+whiteNoise_Fs20kHz_Oct2016.wav';
  wave.frequency = 800; % Hz

  %% Filter coefficients
  filter_1.coefficients = [1 -1];

  %% Impulse response
  filter_2.coefficients = [0.5 0.5 0.5 0.5 0.5 0.3 0.2 0.1];
end
