%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function [waveform_bird, waveform_flute, frame] = setup ()
  %% Provide initial values for WAVEFORM, FILTER_1 and FILTER_2.

  waveform = struct ('filename', '', ...
		 'frequency', 0, ...
		 'data', [], ...
		 'sampling_frequency', 0);

  frame = struct ('length', 0, ...
		  'shift', 0, ...
		  'window_type', '');

  waveform_bird = waveform;
  waveform_flute = waveform;
  waveform_bird.filename = 'sound_bird_labMScIntro.wav';
  waveform_flute.filename = 'sound_flute_labMScIntro.wav';

  frame.length = 10; % ms
  frame.shift = 5; % ms
end
