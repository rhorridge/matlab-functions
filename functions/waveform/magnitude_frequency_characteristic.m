%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function mfc = magnitude_frequency_characteristic (impulse_response)
  %% Compute the magnitude frequency characteristics (MFC) of IMPULSE_RESPONSE.
  full_length = 256;
  impulse_response = ensure_vector_padded_with_zeros (impulse_response, full_length);
  mfc = fft (impulse_response);
