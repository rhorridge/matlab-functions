%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function sample = sample_waveform (waveform, frame, sample)
  %%  Sample WAVEFORM with given FRAME and SAMPLE parameters.
  waveform_length = length (waveform.data);
  number_of_samples = floor ((1000 * waveform_length / waveform.sampling_frequency) / ...
			     (frame.length - frame.shift)); % difference between samples
  sample.data = zeros (number_of_samples, sample.length);
  sample_index = [1:sample.length - sample.offset:waveform_length];

  %% Cannot sample beyond waveform
  while sample_index(end) + sample.length > waveform_length
    sample_index = sample_index(1:end-1);
  end

  sample.times = sample_index * (1000 / waveform.sampling_frequency);
  sample.frequencies = [1:sample.length] * waveform.sampling_frequency / sample.length;

  hamming_window = hamming (sample.length);

  i = 0;
  for index = sample_index
    i = i + 1;
    sample.data(i,:) = get_sample (waveform.data, index, sample.length, hamming_window);
  end
  sample.data = sample.data(1:i,:);
