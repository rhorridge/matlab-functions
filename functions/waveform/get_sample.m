%%%% Copyright (c) 2018 Richard Horridge and Ashley Turner

function sample = get_sample (data, start_index, length, hamming_window)
  %%  Get a sample from DATA of LENGTH, beginning at START_INDEX, using HAMMING_WINDOW.
  sample = hamming_window .* data([start_index:start_index + length - 1]);
