function new_genome = mutate (genome, scheme, probability)
  %% Mutate GENOME with PROBABILITY.
  new_genome = genome;

  if (probability < 0 || probability > 1)
    disp ("Error: Probability must be in range [0 1]");
    return;
  end

  random_number = rand;
  if random_number < probability
    len_genome = length (genome);
    idx_genome = randi (len_genome);
    value = genome(idx_genome);
    encoding = get_encoding_at_genome_index (idx_genome, scheme);

    if strcmp (encoding, 'binary')
      if value == '0'
	value = '1';
      else
	value = '0';
      end
    elseif strcmp (encoding, 'digit')
      number = str2num (value);
      value = num2str (mutate_digit (number));
    end

    new_genome(idx_genome) = value;
  end

end
