function values = decode_genome (genome, scheme)
  %% Decode GENOME according to SCHEME.
  schemes = strsplit (scheme, '|');
  num_values = length (schemes);
  values = cell (num_values, 1);
  for i = 1:num_values
    values{i} = '';
  end
  start_val = 0;
  for i = 1:num_values
    len_schemes_i = length (schemes{i});
    if i == 1
      len_schemes_i_d = 0;
    else
      len_schemes_i_d = scheme_length (schemes{i - 1});
    end
    start_val = start_val + len_schemes_i_d;
    values{i} = decode_single_variable (genome, schemes{i}, start_val);
  end
  values = cellfun (@str2num, values, 'UniformOutput', false);
end
