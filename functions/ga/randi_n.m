function random_integers = randi_n (imax, n)
  %% Generate N random integers in range [1, IMAX].
  if (n > imax)
    n = imax;;
  end

  random_integers = unique (randi (imax, n, 1));

  while (length (random_integers) < n)
    random_integers(end+1) = randi (imax);
    random_integers = unique (random_integers);
  end
end
