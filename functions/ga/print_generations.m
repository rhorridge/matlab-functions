function str = print_generations (generations)
  %% Print iterations of GENERATIONS to STR.
  len_generations = length (generations);
  str = '';
  for i = 1:len_generations
    str = strcat (str, sprintf ('Generation\t%d\n================\n', i));
    len_genomes = length (generations{i});
    for j = 1:len_genomes
      str = strcat (str, sprintf ('Genome\t%d\t%d\t%.2f\t%.1f\t%.2f\t%.2f\t%.4f', ...
	      j, generations{i}{j}.control, generations{i}{j}.braking, ...
	      generations{i}{j}.gain, generations{i}{j}.energy_consumed, ...
	      generations{i}{j}.journey_time, ...
	      generations{i}{j}.fitness), sprintf ('\n'));
    end
    str = strcat (str, sprintf ('=================\n'));
  end
end
