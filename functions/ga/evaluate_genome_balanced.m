function fitness = evaluate_genome_balanced (genome)
  %%  Evaluate GENOME in a balanced way.
  if (genome.journey_time <= 11500)
    fitness = 0.039598808 * genome.energy_consumed + 0.05 * genome.journey_time;
  else
    fitness = 0.039598808 * genome.energy_consumed ...
	      + 0.05 * genome.journey_time ...
	      + 10 * (genome.journey_time - 11500);
  end
end
