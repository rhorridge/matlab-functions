function [genome, scheme] = encode_number_digit (number, num_pre, num_post)
  %% Encode NUMBER by each of its digits with space for NUM_PRE digits before the
  %% decimal point and NUM_POST digits after. Encoding is
  %% little-endian.
  [nums, len_1, len_2] = format_number_decimal (number, num_pre, num_post);

  len_total = len_1 + len_2;
  genome(1:len_total) = '0';

  for i = 1:len_1
    genome(i) = nums{1}(i);
    scheme(i) = 'D';
  end

  scheme(i + 1) = '.';

  for j = 1:len_2
    genome(i + j) = nums{2}(j);
    scheme(i + j + 1) = 'D';
  end
end
