function encoded = encode_digit_bcd (digit)
  %% Encode DIGIT in Binary Coded Decimal (BCD).
  switch digit
    case '0'
      encoded = '0000';
    case '1'
      encoded = '0001';
    case '2'
      encoded = '0010';
    case '3'
      encoded = '0011';
    case '4'
      encoded = '0100';
    case '5'
      encoded = '0101';
    case '6'
      encoded = '0110';
    case '7'
      encoded = '0111';
    case '8'
      encoded = '1000';
    case '9'
      encoded = '1001';
    otherwise
      encoded = '1111';
  end
end
