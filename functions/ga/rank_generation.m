function ranks = rank_generation (generation, key)
  %% Rank a GENERATION according to KEY.
  len_generation = length (generation);
  values = zeros (len_generation, 1);
  for i = 1:len_generation
    values(i) = generation{i}.(key);
  end
  [~, ranks] = sort (values);
end
