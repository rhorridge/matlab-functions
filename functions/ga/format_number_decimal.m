function [numbers, len_1, len_2] = format_number_decimal (number, num_pre, num_post)
    format = strcat ('%', sprintf ('%d', num_pre), '.', sprintf ('%d', num_post), 'f');
    number_fmt = sprintf (format, number);
    numbers = strsplit (number_fmt, '.');
    len = length (numbers);
    len_1 = length (numbers{1});

    if len > 1
      len_2 = length (numbers{2});
    else
      numbers{2} = '';
      len_2 = 0;
    end

    while len_1 < num_pre
      numbers{1} = strcat ('0', numbers{1});
      len_1 = length (numbers{1});
    end

    while len_2 < num_post
      numbers{2} = strcat ('0', numbers{2});
      len_2 = length (numbers{2});
    end
end
