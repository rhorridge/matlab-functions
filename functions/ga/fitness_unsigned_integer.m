function fitness = fitness_unsigned_integer (genome)
  %% Compute FITNESS, the unsigned integer represented by GENOME.
  len_genome = length (genome);
  twos_value = 2 ^ (len_genome - 1);
  fitness = 0;
  for i = 1:len_genome
    if genome(i) == '1'
      fitness = fitness + twos_value;
    end
    twos_value = twos_value / 2;
  end
end
