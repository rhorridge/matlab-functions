function individual = new_genome (individual, generation)
  %% Create a new INDIVIDUAL from GENERATION.
  global train;
  global route;

  vars{1} = 0;
  vars{2} = 0;
  vars{3} = 0;
  while (invalid_range (vars))
    len_generation = length (generation);
    nums = randi_n (len_generation, 2);
    individual.genome = crossover (generation{nums(1)}.genome, generation{nums(2)}.genome);
    individual.genome = mutate (individual.genome, individual.scheme, 0.5);
    vars = decode_genome (individual.genome, individual.scheme);
  end
  individual.control = vars{1};
  individual.braking = vars{2};
  individual.gain = vars{3};
  [individual.energy_consumed, individual.journey_time] = ...
  single_train_simulator (train, route, individual.control, ...
			  individual.braking, ...
			  individual.gain);
  individual.fitness = evaluate_genome (individual, @evaluate_genome_balanced);
end
