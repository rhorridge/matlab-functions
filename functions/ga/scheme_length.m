function len = scheme_length (scheme)
  %% Determine length encoded by SCHEME.
  scheme = strrep (scheme, '.', '');
  len_scheme = length (scheme);
  lengths = '';
  len = 0;
  for i = 1:len_scheme
    if mod (i, 2)
      lengths = strcat (lengths, scheme(i));
    end
  end

  len_lengths = length (lengths);
  for i = 1:len_lengths
    if lengths(i) == 'b'
      len = len + 4;
    else
      len = len + str2num (lengths(i));
    end
  end
end
