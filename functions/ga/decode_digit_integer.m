function digit = decode_digit_integer (values)
  %%  Decode VALUES as a binary integer.
  digit = bin2dec (values);
end
