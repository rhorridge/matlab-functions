function [genome, scheme] = encode_decimal (number, num_pre, num_post)
  %% Encode NUMBER in decimal with space for NUM_PRE digits before the
  %% decimal point and NUM_POST digits after. Encoding is
  %% little-endian.
  [strs, len_strs_1, len_strs_2] = format_number_decimal (number, ...
  num_pre, num_post);

  len_strs_total = len_strs_1 + len_strs_2;
  genome(1:4*len_strs_total) = '0';

  for i = 1:len_strs_1
    genome(4*(i-1) + 1:4*i) = encode_digit_bcd (strs{1}(i));
    scheme(2*i - 1) = 'b';
    scheme(2*i) = 'd';
  end
  scheme(2*i + 1) = '.';
  for j = 1:len_strs_2
    scheme(2*i + 2*j) = 'b';
    scheme(2*i + 2*j + 1) = 'd';
    genome(4*(i + j - 1) + 1:4*(i + j)) = encode_digit_bcd (strs{2}(j));
  end
end
