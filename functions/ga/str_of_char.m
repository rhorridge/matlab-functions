function str = str_of_char (char, number)
  %%  Create a STRing of NUMBER CHARs.
  str(1:number) = char;
end
