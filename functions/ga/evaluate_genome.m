function fitness = evaluate_genome (genome, fn)
  %% Evaluate GENOME using fitness function FN, returning FITNESS.
  fitness = fn (genome);
end
