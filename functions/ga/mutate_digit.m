function new = mutate_digit (digit)
  %%  Mutate DIGIT by increasing or decreasing it by 1.
  %%
  %%  Negatives wrap around to 9.
  if rand () < 0.5
    digit = digit - 1;
    if (digit < 0)
      digit = 9;
    end
  else
    digit = digit + 1;
    if (digit > 9)
      digit = 0;
    end
  end
  new = digit;
end
