function [genome, scheme] = encode_integer (int, max)
  %%  Encode unsigned integer INT, of up to MAX value, in as minimal
  %%  bits as possible. Start from 0.
  number_fmt = int;
  len_encoding = length (dec2bin (max));
  scheme = strcat (sprintf ('%d', len_encoding), 'd');
  genome = dec2bin (int);
  while length (genome) < len_encoding
    genome = strcat ('0', genome);
  end
end
