function yes = invalid_range (vars)
  %% Check if  VARS are in an invalid range
  yes = 0;

  if (vars{1} < 1) || (vars{1} > 3)
    yes = 1;
  end

  if (vars{2} < 0.0) || (vars{2} > 1.0)
    yes = 1;
  end

  if (vars{3} < 0.0) || (vars{3} > 2.0)
    yes = 1;
  end

end
