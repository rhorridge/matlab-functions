function [value, idx_genome, idx_scheme] = decode_at_index ...
					     (genome, scheme, idx_genome, idx_scheme)
  %%  Perform decoding of GENOME using SCHEME at an index given by
  %%  IDX_GENOME and IDX_SCHEME. Returns VALUE, the decoded value
  %%  given by the current indices, and the modified varsions of
  %%  IDX_GENOME and IDX_SCHEME.
  if scheme(idx_scheme) == 'b'
    idx_scheme = idx_scheme + 1;
    value = decode_digit_bcd (genome(idx_genome + 1:idx_genome + 4));
    idx_genome = idx_genome + 4;
    idx_scheme = idx_scheme + 1;
  elseif scheme(idx_scheme) == 'D'
    value = genome(idx_genome + 1);
    idx_genome = idx_genome + 1;
    idx_scheme = idx_scheme + 1;
  elseif isstrprop (count = scheme(idx_scheme), 'digit')
    idx_scheme = idx_scheme + 1;
    count = str2num (count);
    value = num2str (decode_digit_integer (genome(idx_genome + 1:idx_genome + count)));
    idx_genome = idx_genome + count;
    idx_scheme = idx_scheme + 1;
  else
    value = '';
    idx_scheme = idx_scheme + 1;
  end
end
