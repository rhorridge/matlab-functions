function variable =  decode_single_variable (genome, scheme, start_value)
  %% Decode GENOME according to SCHEME, starting from START_VALUE (index
  %% into GENOME before the next starting point of the c urrent
  %% encoding SCHEME.
  len_scheme = length (scheme);
  j = 1;
  i = start_value;
  variable = '';
  while (scheme(j) != '.') && (j < len_scheme)
    [curval, i, j] = decode_at_index (genome, scheme, i, j);
    variable = strcat (variable, curval);
    if (j >= len_scheme)
      break;
    end
  end

  if j < len_scheme
    variable = strcat (variable, '.');
    j = j + 1;
  end

  while (j < len_scheme)
    [curval, i, j] = decode_at_index (genome, scheme, i, j);
    variable = strcat (variable, curval);
  end
end
