function digit = decode_digit_bcd (values)
  %% Decode VALUES as a Binary Coded Decimal (BCD) digit.
  len_values = length (values);
  if len_values != 4
    disp ("Error: VALUES is not a valid BCD digit.");
    digit = '';
  else
    switch values
      case '0000'
	digit = '0';
      case '0001'
	digit = '1';
      case '0010'
	digit = '2';
      case '0011'
	digit = '3';
      case '0100'
	digit = '4';
      case '0101'
	digit = '5';
      case '0110'
	digit = '6';
      case '0111'
	digit = '7';
      case '1000'
	digit = '8';
      case '1001'
	digit = '9';
      otherwise
	disp ("Error: VALUES is not a valid BCD digit.");
	digit = '';
    end
  end
end
