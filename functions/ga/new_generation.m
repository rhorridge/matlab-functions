function retval = new_generation (old_gen)
  %% Create a new generation from OLD_GEN.
  global train;
  global route;

  len_old_gen = length (old_gen);
  mutation_probability = 0.05;
  extra = 2;

  new_gen = cell (len_old_gen + extra, 1);
  len_new_gen = length (new_gen);

  retval = cell (len_old_gen, 1);

  for i = 1:len_new_gen
    new_gen{i} = old_gen{1};
  end

  new_gen = num2cell (cellfun (@(x) new_genome (x, old_gen), new_gen));
  new_gen{1} = old_gen{1};

  ranking = rank_generation (new_gen, "fitness");
  new_gen = new_gen(ranking);

  for i = 1:len_old_gen
    retval{i} = new_gen{i};
  end
end
