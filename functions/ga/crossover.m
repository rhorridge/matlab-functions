function new_genome = crossover (genome_parent_1, genome_parent_2)
  %% Combine GENOME_PARENT_1 and GENOME_PARENT_2 to form a
  %% NEW_GENOME.
  %%
  %% GENOME_PARENT_1 and GENOME_PARENT_2 should be binary vectors of
  %% the same length.

  len_genome = length (genome_parent_1);

  if (len_genome != length (genome_parent_2))
    disp ("Error: Genomes are of different length.");
    new_genome = [];
    return;
  end

  half_len_genome = floor (len_genome);

  idx_genome_parent_1 = randi_n (len_genome, half_len_genome);

  new_genome = genome_parent_2;
  new_genome(idx_genome_parent_1) = genome_parent_1(idx_genome_parent_1);
end
