function encoding = get_encoding_at_genome_index (index, scheme)
  %%  Get the ENCODING at INDEX into the genome defined by SCHEME.
  len_scheme = length (scheme);
  representations = '';
  i = 1;
  prefix = '';
  while i <= len_scheme
    encoding = scheme(i);
    if encoding == 'b'
      prefix = 'b';
      i = i + 1;
    elseif encoding == 'D'
      representations = strcat (representations, 'D');
      i = i + 1;
    elseif isstrprop (scheme(i), 'digit')
      prefix = str2num (scheme(i));
      i = i + 1;
    elseif encoding == 'd'
      if prefix == 'b'
	representations = strcat (representations, str_of_char ('b', prefix));
      elseif isnumeric (prefix)
	representations = strcat (representations, str_of_char ('b', prefix));
      end
      i = i + 1;
    elseif encoding == '|'
      i = i + 1;
    elseif encoding == '.'
      i = i + 1;
    else
      warning ('Unknown encoding scheme');
    end
  end

  encoding = representations(index);

  if encoding == 'b'
    encoding = 'binary';
  elseif encoding == 'D'
    encoding = 'digit';
  elseif isstrprop (encoding, 'digit')
    encoding = 'binary';
  else
    encoding = 'unknown';
  end
end
